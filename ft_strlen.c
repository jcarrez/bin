/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/05 18:36:26 by jcarrez           #+#    #+#             */
/*   Updated: 2020/06/05 19:14:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//
// Count the lenght of the argv send
//

#include "unistd.h"

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_put_st(size_t nb)
{
	if (nb > 9)
	{
		ft_put_st(nb / 10);
		ft_put_st(nb % 10);
	}
	else
		ft_putchar(nb + '0');
}

size_t	ft_put_str(char *s)
{
	size_t	nb;

	nb = 0;
	while (s[nb])
		ft_putchar(s[nb++]);
	return (nb);
}

int		main(int argc, char **argv)
{
	size_t	nb;

	nb = 0;
	if (argc == 2)
	{
		while (argv[1][nb])
			nb++;
		ft_put_st(nb);
	}
	else
	{
		ft_put_str("Usage: ft_strlen <string>");
	}
	ft_putchar('\n');
	return (0);
}
