# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    cls                                                :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/06/05 18:33:10 by jcarrez           #+#    #+#              #
#    Updated: 2020/06/05 19:12:56 by jcarrez          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

##
## detach zsh and clear screen when exit
##

screen & zsh
clear
